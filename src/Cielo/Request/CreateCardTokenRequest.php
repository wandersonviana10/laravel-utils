<?php

namespace BaseProject\Cielo\Request;

use Cielo\API30\Ecommerce\Environment;
use Cielo\API30\Ecommerce\Sale;
use Cielo\API30\Merchant;
use BaseProject\Cielo\CardToken;

class CreateCardTokenRequest extends AbstractRequest
{

    private $environment;

    public function __construct(Merchant $merchant, Environment $environment)
    {
        parent::__construct($merchant);

        $this->environment = $environment;
    }

    public function execute($card)
    {
        $url = $this->environment->getApiUrl() . '1/card/';

        return $this->sendRequest('POST', $url, $card);
    }

    protected function unserialize($json)
    {
        return CardToken::fromJson($json);
    }
}