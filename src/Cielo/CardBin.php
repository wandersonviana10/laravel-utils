<?php


namespace BaseProject\Cielo;

class CardBin implements \JsonSerializable
{
    private $status;
    private $provider;
    private $cardType;
    private $foreignCard;
    private $corporateCard;
    private $issuer;
    private $issuerCode;

    public static function fromJson($json)
    {
        $object = json_decode($json);

        $sale = new CardBin();
        $sale->populate($object);

        return $sale;
    }

    public function jsonSerialize()
    {
        return get_object_vars($this);
    }

    public function populate(\stdClass $data)
    {
        $this->status = $data->Status ?? null;
        $this->provider = $data->Provider ?? null;
        $this->cardType = $data->CardType ?? null;
        $this->foreignCard = $data->ForeignCard ?? null;
        $this->corporateCard = $data->CorporateCard ?? null;
        $this->issuer = $data->Issuer ?? null;
        $this->issuerCode = $data->IssuerCode ?? null;
    }


    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return mixed
     */
    public function getProvider()
    {
        return $this->provider;
    }

    /**
     * @return mixed
     */
    public function getCardType()
    {
        return $this->cardType;
    }

    /**
     * @return mixed
     */
    public function getForeignCard()
    {
        return $this->foreignCard;
    }

    /**
     * @return mixed
     */
    public function getCorporateCard()
    {
        return $this->corporateCard;
    }

    /**
     * @return mixed
     */
    public function getIssuer()
    {
        return $this->issuer;
    }

    /**
     * @return mixed
     */
    public function getIssuerCode()
    {
        return $this->issuerCode;
    }


}