<?php

namespace BaseProject\Cielo;


class CardToken implements \JsonSerializable
{
    private $cardToken;

    private $links;

    public static function fromJson($json)
    {
        $object = json_decode($json);

        $sale = new CardToken();
        $sale->populate($object);

        return $sale;
    }

    public function jsonSerialize()
    {
        return get_object_vars($this);
    }

    public function populate(\stdClass $data)
    {
        $this->cardToken = isset($data->CardToken)? $data->CardToken: null;
        $this->links = isset($data->Links)? $data->Links: [];
    }

    /**
     * @return mixed
     */
    public function getCardToken()
    {
        return $this->cardToken;
    }

    /**
     * @return mixed
     */
    public function getLinks()
    {
        return $this->links;
    }
}