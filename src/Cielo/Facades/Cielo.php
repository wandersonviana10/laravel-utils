<?php

namespace BaseProject\Cielo\Facades;

use Illuminate\Support\Facades\Facade;

class Cielo extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'cielo';
    }
}