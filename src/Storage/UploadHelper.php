<?php

namespace BaseProject\Storage;

use Illuminate\Support\Str;
use Illuminate\Http\UploadedFile;
use Intervention\Image\ImageManager;

class UploadHelper
{

    static function uploadImage(
        UploadedFile $file,
        $path,
        $width = 0,
        $height = 0,
        $quality = 90,
        $disk = null) {

        $extension = $file->getClientOriginalExtension();

        $storage = \Storage::disk($disk);
        $filename = Str::random(40) . "." . $extension;
        $resultPath = $path . "/" . $filename;

        if (in_array($extension, ['png', 'jpg', 'gif', 'webp']) && class_exists(ImageManager::class)) {
            $manager = new ImageManager(array('driver' => env('IMAGE_DRIVER', 'gd')));
            $image = $manager->make($file);
            if ($width > 0 && $height > 0) {
                $image->fit($width, $height);
            } elseif ($width <= 0 && $height > 0) {
                $image->heighten($height);
            } elseif ($height <= 0 && $width > 0) {
                $image->widen($width);
            }
            $contents = $image->stream($extension, $quality ?? 90)->__toString();

            $storage->put($resultPath, $contents);
        } else {
            $resultPath = $storage->putFileAs($path, $file, $filename, ['ContentType' => $file->getMimeType()]);
        }

        return $storage->url($resultPath);
    }
}