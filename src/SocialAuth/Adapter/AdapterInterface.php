<?php
/**
 * AdapterInterface.php
 * Gynbus Lite
 *
 * Copyright © 2016 BaseProject. All rights reserved.
 */

namespace BaseProject\SocialAuth\Adapter;


use BaseProject\SocialAuth\Exception\HttpResponseException;

interface AdapterInterface
{
    public function getAuthUrl();

    /**
     * @return boolean
     */
    public function authenticate();

    /**
     * @param null $key
     * @return array|null
     */
    public function getUserInfo($key = null);

    /**
     * @param null $accessToken
     * @return array|null
     */
    public function me($accessToken = null);

    /**
     * @return array
     */
    public function prepareAuthParams();


    /**
     * @throws HttpResponseException
     */
    public function makeResponseException($decodedResponse);
}