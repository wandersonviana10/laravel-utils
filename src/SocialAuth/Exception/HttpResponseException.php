<?php
/**
 * HttpResponseException.php
 * Gynbus Lite
 * Copyright © 2016 BaseProject. All rights reserved.
 */

namespace BaseProject\SocialAuth\Exception;


class HttpResponseException extends \Exception
{
    protected $responseData;

    public function __construct($message, $code, $responseData)
    {
        parent::__construct($message, $code);
        $this->responseData = $responseData;
    }

    public function getResponseData()
    {
        return $this->responseData;
    }
}