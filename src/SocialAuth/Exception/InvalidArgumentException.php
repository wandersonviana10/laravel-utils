<?php
/**
 * InvalidArgumentException.php
 * Copyright © 2016 BaseProject. All rights reserved.
 */

namespace BaseProject\SocialAuth\Exception;


class InvalidArgumentException extends \InvalidArgumentException
{

}