<?php

namespace BaseProject\SocialAuth;


class Facade extends \Illuminate\Support\Facades\Facade
{

    protected static function getFacadeAccessor()
    {
        return 'social-auth';
    }
}