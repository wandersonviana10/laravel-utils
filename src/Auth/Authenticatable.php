<?php

namespace BaseProject\Auth;


trait Authenticatable
{
    use \Illuminate\Auth\Authenticatable;

    public function logout()
    {
        if ($this instanceof \BaseProject\Auth\Contract\HasAccessToken && $this->accessTokens->count()) {
            $this->accessTokens->each->delete();
        }
    }
}