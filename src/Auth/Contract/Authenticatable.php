<?php

namespace BaseProject\Auth\Contract;


interface Authenticatable extends \Illuminate\Contracts\Auth\Authenticatable
{

    public function logout();
}