<?php

namespace BaseProject\Auth\Contract;

use Illuminate\Database\Eloquent\Relations\MorphMany;
use BaseProject\Auth\AccessToken;


interface HasAccessToken
{
    /**
     * Get the name of the unique identifier for the user.
     *
     * @return MorphMany|AccessToken[]
     */
    public function accessTokens();
}