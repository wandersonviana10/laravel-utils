<?php

namespace BaseProject\Utils\Contract;


interface HasDevicesContract
{
    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function devices();
}