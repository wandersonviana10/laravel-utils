<?php

namespace BaseProject\Utils\Contract;

/**
 * Interface HasDeviceToken
 * @package BaseProject\Utils\Contract
 * @property string $device_token
 */
interface HasDeviceToken
{

}