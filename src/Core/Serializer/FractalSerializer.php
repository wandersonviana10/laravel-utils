<?php

namespace BaseProject\Utils\Serializer;


use League\Fractal\Serializer\ArraySerializer;

class FractalSerializer extends ArraySerializer
{
    /**
     * Serialize a collection
     *
     * @param string $resourceKey
     * @param  array $data
     * @return array
     */
    public function collection($resourceKey, array $data)
    {
        return $resourceKey == 'empty' ? $data : [$resourceKey ?: 'items' => $data];
    }

    /**
     * Serialize an item
     *
     * @param string $resourceKey
     * @param  array $data
     * @return array
     */
    public function item($resourceKey, array $data)
    {
        return $resourceKey == 'empty' ? $data : [$resourceKey ?: 'item' => $data];
    }
}
