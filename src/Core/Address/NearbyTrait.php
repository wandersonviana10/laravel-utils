<?php

namespace BaseProject\Utils\Address;


trait NearbyTrait
{

    public function scopeDistantCep($query, $cep) {

        $cep = preg_replace('/\D/', '', $cep);
        if (strlen($cep) == 8) {
            $cep = preg_replace("/(\d{5})(\d{3})/", "$1-$2", $cep);
        }

        $searchedTable = 'loc_addresses';
        $cepInfo = \DB::table($searchedTable)
            ->where('cep', $cep)->first();

        if (!$cepInfo) {
            $searchedTable = 'loc_cities';
            $cepInfo = \DB::table($searchedTable)
                ->where('cep', $cep)->first();
        }

        $latitude = null;
        $longitude = null;

        if (!isset($cepInfo->latitude) || !isset($cepInfo->longitude)) {

            $address = \Geocoder::geocode($cep);

            if ($address) {
                if($cepInfo) {
                    \DB::table($searchedTable)
                        ->where('id', $cepInfo->id)
                        ->update([
                            'latitude' => $address->latitude,
                            'longitude' => $address->longitude
                        ]);
                }

                $latitude = $address->latitude;
                $longitude = $address->longitude;
            }
        } else {
            $latitude = $cepInfo->latitude;
            $longitude = $cepInfo->longitude;
        }

        if (!$latitude || !$longitude) {
            throw new \InvalidArgumentException("Não conseguimos detectar sua localização, verifique seu CEP, e ou tente cadastrar um endereço usando sua localização");
        }


        return $query->distantLocation($latitude, $longitude);
    }

    public function scopeDistantLocation($query, $lat, $lng) {

        $distanceUnit = 111.045;

        $table = $this->getTable();

        $lat = floatval($lat);
        $lng = floatval($lng);

        if (!($lat >= -90 && $lat <= 90)) {
            throw new \Exception("Latitude must be between -90 and 90 degrees.");
        }

        if (!($lng >= -180 && $lng <= 180)) {
            throw new \Exception("Longitude must be between -180 and 180 degrees.");
        }

        $haversine = sprintf('%s.*, (%f * DEGREES(ACOS(COS(RADIANS(%f)) * COS(RADIANS(latitude)) * COS(RADIANS(%f - longitude)) + SIN(RADIANS(%f)) * SIN(RADIANS(latitude))))) AS distance',
            $table,
            $distanceUnit,
            $lat,
            $lng,
            $lat
        );

        $subselect = clone $query;
        $subselect
            ->selectRaw(\DB::raw($haversine));

        $query
            ->from(\DB::raw('(' . $subselect->toSql() . ') as ' . $table));

        return $query;
    }

    public function scopeNearby($query)
    {
        return $query->whereRaw('distance <= radius_service');
    }
}