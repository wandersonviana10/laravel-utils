<?php

namespace BaseProject\Utils\Requests;

use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Foundation\Http\FormRequest as BaseFormRequest;
use Illuminate\Support\Carbon;

class FormRequest extends BaseFormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [];
    }

    public function get($key, $default = null)
    {
        return data_get($this->all(), $key);
    }

    protected function prepareForValidation()
    {
        $input = $this->parse($this->all());
        $this->replace($input);
    }

    protected function parse(array $attributes = array()) {
        if (isset($attributes['cpf_cnpj'])) {
            $attributes['cpf_cnpj'] = preg_replace('/\D/', '', $attributes['cpf_cnpj']);
        }

        if (isset($attributes['bank_cpf_cnpj'])) {
            $attributes['bank_cpf_cnpj'] = preg_replace('/\D/', '', $attributes['bank_cpf_cnpj']);
        }

        if (isset($attributes['phone_number'])) {
            $attributes['phone_number'] = preg_replace('/\D/', '', $attributes['phone_number']);
        }

        if (isset($attributes['cep'])) {
            $attributes['cep'] = preg_replace('/\D/', '', $attributes['cep']);
        }

        if (isset($attributes['birthdate'])) {
            $attributes['birthdate'] = $this->parseDate($attributes['birthdate']);
        }
        if (isset($attributes['valid_at'])) {
            $attributes['valid_at'] = $this->parseDate($attributes['valid_at']);
        }

        if (isset($attributes['expire_at'])) {
            $attributes['expire_at'] = $this->parseDate($attributes['expire_at']);
        }

        return $attributes;
    }

    protected function parseDate($attribute) {
        if (!isset($attribute) || !is_string($attribute)) return null;

        $formats = [
            [
                'regex' => '/\d{4}\-\d{2}\-\d{2}/',
                'format' => 'Y-m-d'
            ], [
                'regex' => '/\d{2}\/\d{2}\/\d{4}/',
                'format' => 'd/m/Y'
            ],[
                'regex' => '/\d{2}\/\d{2}\/\d{4} \d{2}\:\d{2}:\d{2}/',
                'format' => 'd/m/Y H:i:s'
            ],[
                'regex' => '/\d{4}\-\d{2}\-\d{2} \d{2}\:\d{2}:\d{2}/',
                'format' => 'Y-m-d H:i:s'
            ]
        ];

        $parsed = null;
        foreach ($formats as $format) {
            try {
                if(preg_match($format['regex'], $attribute)) {
                    $parsed = Carbon::createFromFormat($format['format'], $attribute);
                    if ($parsed) break;
                }
            } catch (\Exception $e){}
        }

        if (is_null($parsed) && ($timestamp = strtotime($attribute))) {
            $parsed = Carbon::createFromTimestamp($timestamp, new \DateTimeZone( 'UTC' ));
        }

        return $parsed;
    }

    protected function failedAuthorization() {
        throw new AuthorizationException('Você não possui autorização para acessar essa ação.');
    }
}