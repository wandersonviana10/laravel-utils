<?php

namespace BaseProject\Utils\Requests;

use App\Http\Requests\FormRequest;
use App\Models\Device;
use Illuminate\Validation\ValidationException;

class ExtendedRequest extends FormRecompsquest
{
    public $device = null;

    public function user($guard = null)
    {
        $guards = [null, 'api', 'api_client', 'api_supplier'];

        foreach ($guards as $guard) {
            if ($user = parent::user($guard)) {
                return $user;
            }
        }

        return parent::user();
    }

    protected function parse(array $attributes = array())
    {
        $attributes = parent::parse($attributes);

        if($this->header('Installation') && ($installation = \GuzzleHttp\json_decode($this->header('Installation'), true))) {
            if ($installation['device_id'] && $installation['platform'])
                $this->device = Device::where('device_id', $installation['device_id'])
                    ->where('platform', $installation['platform'])->first();
        }
        return $attributes;
    }

    public function verifyUserPassword()
    {
        if (isset($this->user_password) && !empty($this->user_password)) {
            $currentPassword = $this->user()->getAuthPassword();
            if (\Hash::check($this->user_password, $currentPassword)) {
                return true;
            }
        }
        throw ValidationException::withMessages(['Senha inserida inválida, a senha deve ser identica à senha do administrador logado.']);
    }
}
