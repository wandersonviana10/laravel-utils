<?php

namespace BaseProject\Utils\Traits;


use Illuminate\Database\QueryException;

trait ReferUserTrait
{
    public function getReferrerCode()
    {
        return $this->referrer_code;
    }

    public function getReferredCode()
    {
        return $this->referred_code;
    }

    public static function generateReferrerCode($stringBase)
    {
        $stringBase = substr(\Str::slug($stringBase, ""), 0, 6);

        try {
            $tries = 0;
            $level = 0;
            do {
                $code = $stringBase . rand(1, 9) . \Str::random(3);
                $findedUser = self::where('referrer_code', $code)->first();
                $tries++;

                if ($tries > 10) {
                    $stringBase = $stringBase . \Str::random(1);
                    $tries = 0;
                    $level++;
                }
            } while ($findedUser);
        } catch (QueryException $e) {
            $code = \Str::random(20);
        }

        return mb_strtolower($code);
    }
}
