<?php

namespace BaseProject\Geocoder;

use BaseProject\Geocoder\Adapter\GoogleAdapter;
use BaseProject\Geocoder\Adapter\HereAdapter;

class Geocoder
{
    const GOOGLE = 'google';
    const HERE = 'here';

    protected $adapterName = self::GOOGLE;

    protected $adapter;

    protected $config;

    public function __construct($config)
    {
        $this->config = $config;
    }

    private function getAdapter() {
        if ($this->adapterName == self::GOOGLE) {
            $this->adapter = new GoogleAdapter($this->config[$this->adapterName]);
        } else {
            $this->adapter = new HereAdapter($this->config[$this->adapterName]);
        }

        return $this->adapter;
    }

    public function use($adapter = self::GOOGLE) {
        $this->adapterName = $adapter;
        return $this;
    }

    public function reverse($lat, $lng)
    {
        $adapter = new GoogleAdapter($this->config[self::GOOGLE]);
        $address = $adapter->reverse($lat, $lng);

        if (!$address || $address->isIncomplete()) {
            $adapter = new HereAdapter($this->config[self::HERE]);
            $address2 = $adapter->reverse($lat, $lng);

            if (!$address) {
                return $address2;
            }

            $address->country = $address->country ?? $address2->country?? null;
            $address->state = $address->state ?? $address2->state?? null;
            $address->city = $address->city ?? $address2->city?? null;
            $address->neighborhood = $address->neighborhood ?? $address2->neighborhood?? null;
            $address->route = $address->route ?? $address2->route?? null;
            $address->number = $address->number ?? $address2->number?? null;
            $address->route = $address->route ?? $address2->route?? null;

            if (strlen($address2->postalcode ?? null) > strlen($address->postalcode ?? null)) {
                $address->postalcode = $address2->postalcode;
            }
            return $address;
        }

        return $address;
    }

    public function geocode($cep)
    {
        $adapter = new GoogleAdapter($this->config[self::GOOGLE]);
        $address = $adapter->geocode($cep);

        if (!$address || $address->isIncomplete()) {
            $adapter = new HereAdapter($this->config[self::HERE]);
            $address2 = $adapter->geocode($cep);

            if (!$address) {
                return $address2;
            }

            $address->country = $address->country ?? $address2->country ?? null;
            $address->state = $address->state ?? $address2->state ?? null;
            $address->city = $address->city ?? $address2->city ?? null;
            $address->neighborhood = $address->neighborhood ?? $address2->neighborhood ?? null;
            $address->route = $address->route ?? $address2->route ?? null;
            $address->number = $address->number ?? $address2->number ?? null;
            $address->route = $address->route ?? $address2->route ?? null;

            if (strlen($address2->postalcode ?? null) > strlen($address->postalcode ?? null)) {
                $address->postalcode = $address2->postalcode;
            }
            return $address;
        }

        return $address;
    }

    public function __call($name, $parameters)
    {

        $adapter = $this->getAdapter();
        $this->use(self::GOOGLE);

        return $adapter->{$name}(...$parameters);
    }
}