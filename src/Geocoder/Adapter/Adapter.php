<?php
/**
 * Created by PhpStorm.
 * Date: 20/03/18
 * Time: 15:41
 */

namespace BaseProject\Geocoder\Adapter;


use BaseProject\Geocoder\Address;

abstract class Adapter
{
    protected $config;

    public function __construct(array $config)
    {
        $this->config = $config;
    }

    protected abstract function defaultParams(): array;

    public abstract function reverse($lat, $lng);

    public abstract function geocode($address);

    protected function call($url, $parameters = []) {
        $parameters = array_merge($this->defaultParams(), $parameters);
        $url = $url . "?" . http_build_query($parameters);

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER      => 1,
            CURLOPT_URL                 => $url,
            CURLOPT_SSL_VERIFYPEER      => false,
            CURLOPT_FAILONERROR         => true,
        ));
        $request = curl_exec($curl);
        if (empty($request)) {
            throw new \RuntimeException('cURL request retuened following error: '.curl_error($curl) );
        }
        curl_close($curl);

        return json_decode($request, true);
    }
}